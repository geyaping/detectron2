## Unit Tests

To run the unittests, do:
```
cd detectron2
python -m unittest discover -v -s ./tests
```

There are also end-to-end inference & training tests, in [dev/run_*_tests.sh](../dev).

you need to place the test_window.py file which is provided here
https://github.com/yogeshkumarpilli/detectron2/blob/master/tests/test_window.py